# OMS

## Резюме

Название: OMS  
Домен: OMS  
Назначение: Сервис управления заказами  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

### Конфигурация системы оплаты Сбербанк СПБ

После выпуска сертификата для системы оплаты Сбербанк в формате p12 его необходимо распаковать для получения клиентского сертификата и приватного ключа.
Это необходимо для того, чтобы в OpenSSL 3 не включать явную загрузку устаревших алгоритмов.

Для этого выполните команды c ключем *-legacy* для OpenSSL 3 (при выполнении команды будет запрошен пароль от архива):

`openssl pkcs12 -in {имя_сертификата}.p12 -clcerts -legacy -nokeys -out client_cert.crt`

`openssl pkcs12 -in {имя_сертификата}.p12 -nodes -legacy -nocerts -out private.key`

По умолчанию полученные файлы необходимо разместить по пути *var/www/storage/sber*.
С помощью env переменной **SBER_CERT_PATH** можно изменить путь для размещения файлов.

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
| --- | --- | --- |
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений. <br/>Producer осуществляет запись в следующие топики:<br/> - `<контур>.orders.fact.orders.1`<br/> - `<контур>.orders.fact.deliveries.1`<br/> Consumer слушает следующие топики:<br/> - `<контур>.logistic.fact.delivery-orders.1` | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |
| Logistic | Ensi Logistic | LOGISTIC_LOGISTIC_SERVICE_HOST |
| Catalog | Ensi Offers<br/>Ensi PIM | CATALOG_OFFERS_SERVICE_HOST<br/>CATALOG_PIM_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/oms/job/oms/  
URL: https://oms-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
