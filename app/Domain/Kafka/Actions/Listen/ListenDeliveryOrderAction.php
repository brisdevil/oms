<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\DeliveryOrder\DeliveryOrderEventMessage;
use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use RdKafka\Message;

class ListenDeliveryOrderAction
{
    public function execute(Message $message): void
    {
        $eventMessage = DeliveryOrderEventMessage::makeFromRdKafka($message);
        if ($eventMessage->event == DeliveryOrderEventMessage::UPDATE && in_array('status', $eventMessage->dirty)) {
            /** @var Delivery|null $delivery */
            $delivery = Delivery::query()->find($eventMessage->attributes->delivery_id);
            if (!$delivery) {
                return;
            }

            $deliveryStatus = DeliveryStatus::matchFromDeliveryOrder($eventMessage->attributes->status);
            if ($deliveryStatus) {
                $delivery->status = $deliveryStatus->id;
            }

            if ($delivery->isDirty('status')) {
                $delivery->save();
                DeliveryStatusUpdated::dispatch($delivery);
            }
        }
    }
}
