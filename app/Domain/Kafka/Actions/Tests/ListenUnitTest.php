<?php

use App\Domain\Kafka\Actions\Listen\ListenDeliveryOrderAction;
use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Messages\Listen\DeliveryOrder\DeliveryOrderEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\DeliveryOrderEventMessageFactory;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Ensi\LogisticClient\Dto\DeliveryOrderStatusEnum;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit', 'kafka');

// region ListenDeliveryOrderAction
test("Action ListenDeliveryOrderAction success", function (string $event, int $deliveryStatus, ?array $dirty, DeliveryStatusEnum $status) {
    /** @var IntegrationTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->forApprovedOrder()->create(['status' => DeliveryStatusEnum::ASSEMBLED]);
    $message = DeliveryOrderEventMessageFactory::new()->make([
        'event' => $event,
        'attributes' => [
            'delivery_id' => $delivery->id,
            'status' => $deliveryStatus,
        ],
        'dirty' => $dirty,
    ]);

    if ($status != $delivery->status) {
        $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');
        Event::fake();
    }

    resolve(ListenDeliveryOrderAction::class)->execute($message);

    if ($status != $delivery->status) {
        Event::assertDispatched(DeliveryStatusUpdated::class);
    }

    assertDatabaseHas(Delivery::class, [
        'id' => $delivery->id,
        'status' => $status->value,
    ]);
})->with([
    'change' => [DeliveryOrderEventMessage::UPDATE, DeliveryOrderStatusEnum::SHIPPED, ['status'], DeliveryStatusEnum::TRANSFER],
    'dont change event' => [DeliveryOrderEventMessage::CREATE, DeliveryOrderStatusEnum::SHIPPED, null, DeliveryStatusEnum::ASSEMBLED],
    'dont change dirty' => [DeliveryOrderEventMessage::UPDATE, DeliveryOrderStatusEnum::SHIPPED, ['id'], DeliveryStatusEnum::ASSEMBLED],
]);
// endregion
