<?php

namespace App\Domain\Orders\Actions\Delivery;

use App\Domain\Orders\Data\Timeslot;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;

class PatchDeliveryAction
{
    public function execute(int $deliveryId, array $fields): Delivery
    {
        if (isset($fields['timeslot'])) {
            $fields['timeslot'] = Timeslot::validateOrFail($fields['timeslot']);
        }
        /** @var Delivery $delivery */
        $delivery = Delivery::query()->findOrFail($deliveryId);
        $delivery->fill($fields);

        $delivery->save();

        if ($delivery->wasChanged('status')) {
            DeliveryStatusUpdated::dispatch($delivery);
        }

        $delivery->setRelations([]);

        return $delivery;
    }
}
