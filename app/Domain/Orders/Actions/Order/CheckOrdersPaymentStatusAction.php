<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CheckPaymentAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;

class CheckOrdersPaymentStatusAction
{
    protected LoggerInterface $logger;

    public function __construct(protected CheckPaymentAction $checkPaymentAction)
    {
        $this->logger = logger()->channel('checkout');
    }

    public function execute(): void
    {
        $orders = Order::query()
            ->whereNotNull('payment_external_id')
            ->whereIn('payment_status', [PaymentStatusEnum::NOT_PAID, PaymentStatusEnum::HOLD])
            ->get();

        /** @var Order $order */
        foreach ($orders as $order) {
            try {
                $this->checkPaymentAction->execute($order);
            } catch (Exception $e) {
                $this->logger->error("Ошибка получения статуса заказа {$order->id}", ['message' => $e->getMessage()]);
            }
        }
    }
}
