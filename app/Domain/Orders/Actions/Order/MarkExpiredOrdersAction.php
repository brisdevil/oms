<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Date;

class MarkExpiredOrdersAction
{
    public function __construct(
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    public function execute(): void
    {
        $minutes = (int)Setting::getValue(SettingCodeEnum::PROCESSING);

        /** @var Collection|Order[] $orders */
        $orders = Order::query()
            ->where('status', OrderStatusEnum::NEW)
            ->where('created_at', '<', Date::now()->subMinutes($minutes)->format('Y-m-d H:i:s'))
            ->get();

        foreach ($orders as $order) {
            $order->is_expired = true;
            $this->saveOrderAction->execute($order);
        }
    }
}
