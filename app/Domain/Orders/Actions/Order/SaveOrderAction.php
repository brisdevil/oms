<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Events\OrderPaymentUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use Illuminate\Support\Facades\DB;

class SaveOrderAction
{
    public function execute(Order $order): void
    {
        if ($order->isDirty('status')) {
            if ($order->status == OrderStatusEnum::APPROVED) {
                $this->checkCanApprove($order);
            }
        }

        DB::transaction(function () use ($order) {
            $order->save();

            OrderPaymentUpdated::dispatchIfChanged($order);
            OrderStatusUpdated::dispatchIfChanged($order);
        });
    }

    protected function checkCanApprove(Order $order): void
    {
        if ($order->payed_price > 0 && $order->payed_price < $order->price) {
            throw new ValidateException("Итоговая стоимость заказ не может превышать размер совершенной оплаты ({$order->payed_price} коп.)");
        }
    }
}
