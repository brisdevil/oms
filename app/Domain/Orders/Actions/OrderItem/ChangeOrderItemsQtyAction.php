<?php

namespace App\Domain\Orders\Actions\OrderItem;

use App\Domain\Orders\Actions\Order\CalcOrderPriceAction;
use App\Domain\Orders\Actions\Order\LoadForEditOrderAction;
use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentsAction;
use App\Domain\Orders\Models\OrderItem;
use App\Exceptions\ValidateException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ChangeOrderItemsQtyAction
{
    public function __construct(
        protected CalcShipmentsAction $calcShipmentsAction,
        protected LoadForEditOrderAction $loadOrderAction,
        protected CalcOrderPriceAction $calcOrderPriceAction,
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    /**
     * @throws ValidateException
     */
    public function execute(int $orderId, array $fields): array
    {
        $order = $this->loadOrderAction->execute($orderId);

        $orderItemsData = Arr::pluck($fields['order_items'], 'qty', 'item_id');

        /** @var Collection|OrderItem[] $orderItems */
        $orderItems = OrderItem::query()
            ->with(['shipment'])
            ->where('order_id', $orderId)
            ->whereIn('id', array_keys($orderItemsData))
            ->get();

        if (count($orderItemsData) !== $orderItems->count()) {
            throw new ValidateException("Не найдены все товары с такими параметрами");
        }

        return DB::transaction(function () use ($orderItemsData, $orderItems, $order) {
            $shipments = [];
            $result = [];

            foreach ($orderItems as $orderItem) {
                if (is_null($orderItem->old_qty)) {
                    $orderItem->old_qty = $orderItem->qty;
                }
                $orderItem->qty = (float)$orderItemsData[$orderItem->id];
                $orderItem->save();

                $shipments[$orderItem->shipment->id] = $orderItem->shipment;
                $result[] = $orderItem;
            }

            $this->calcShipmentsAction->execute($shipments);

            $this->calcOrderPriceAction->execute($order->load('items'));
            $order->is_changed = true;
            $this->saveOrderAction->execute($order);

            return $result;
        });
    }
}
