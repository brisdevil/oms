<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Models\Shipment;
use Illuminate\Database\Eloquent\Collection;

class CalcShipmentsAction
{
    public function __construct(
        protected CalcShipmentAction $calcShipmentAction,
    ) {
    }

    public function execute(array $shipments): void
    {
        /** @var Collection<Shipment> $shipments */
        $shipments = new Collection($shipments);
        $shipments->loadMissing('orderItems');
        foreach ($shipments as $shipment) {
            $this->calcShipmentAction->execute($shipment);
        }
    }
}
