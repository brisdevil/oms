<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Models\Shipment;

class PatchShipmentAction
{
    public function execute(int $shipmentId, array $fields): Shipment
    {
        /** @var Shipment $shipment */
        $shipment = Shipment::query()->findOrFail($shipmentId);
        $shipment->fill($fields);

        $shipment->save();

        if ($shipment->wasChanged('status')) {
            ShipmentStatusUpdated::dispatch($shipment);
        }

        $shipment->setRelations([]);

        return $shipment;
    }
}
