<?php

namespace App\Domain\Orders\Actions\UpsertOrders\AddOrderItems;

use App\Domain\Orders\Actions\Order\CalcOrderPriceAction;
use App\Domain\Orders\Actions\Order\LoadForEditOrderAction;
use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentsAction;
use App\Domain\Orders\Actions\UpsertOrders\AddOrderItems\Data\AddOrderItemsContext;
use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use App\Domain\Orders\Actions\UpsertOrders\Stages\LoadProductInfoAction;
use App\Domain\Orders\Actions\UpsertOrders\Stages\MakeOrderItemAction;
use App\Domain\Orders\Actions\UpsertOrders\Stages\MakeShipmentAction;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\Shipment;
use App\Exceptions\ValidateException;
use Ensi\OffersClient\Dto\Stock;
use Exception;
use Illuminate\Support\Facades\DB;

class AddOrderItemsAction
{
    public function __construct(
        protected LoadForEditOrderAction $loadOrderAction,
        protected LoadProductInfoAction $loadProductInfoAction,
        protected MakeOrderItemAction $makeOrderItemAction,
        protected MakeShipmentAction $makeShipmentAction,
        protected CalcShipmentsAction $calcShipmentsAction,
        protected CalcOrderPriceAction $calcOrderPriceAction,
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    /**
     * @param OrderItemData[] $orderItems
     */
    public function execute(int $orderId, array $orderItems): Order
    {
        $context = new AddOrderItemsContext($orderItems);

        try {
            $order = $this->loadOrderAction->execute($orderId);
            $order->loadMissing('deliveries.shipments');

            $context->setOrder($order);

            $this->loadProductInfoAction->execute($context);

            DB::transaction(function () use ($context) {
                $updatedShipments = [];

                foreach ($context->items() as $itemData) {
                    $shipment = $this->getShipmentForStore($context, $itemData);

                    $productInfo = $context->getProductInfo($itemData->offerId);
                    $itemData->costPerOne = $productInfo->offer->getBasePrice();
                    $itemData->pricePerOne = $productInfo->offer->getBasePrice();
                    $orderItem = $this->makeOrderItemAction->execute($itemData, $productInfo, $shipment, $context->order);
                    $orderItem->is_added = true;
                    $orderItem->save();

                    $updatedShipments[$shipment->id] = $shipment;
                }

                $this->calcShipmentsAction->execute($updatedShipments);

                $this->calcOrderPriceAction->execute($context->order->load('items'));
                $context->order->is_changed = true;
                $this->saveOrderAction->execute($context->order);
            });

            $context->order->refresh();

            return $context->order;
        } catch (ValidateException $e) {
            $context->logger->warning($e->getMessage());

            throw $e;
        } catch (Exception $e) {
            $context->logger->error($e->getMessage(), [
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);

            throw $e;
        }
    }

    protected function getShipmentForStore(AddOrderItemsContext $context, OrderItemData $itemData): Shipment
    {
        $productInfo = $context->getProductInfo($itemData->offerId);

        foreach ($productInfo->offer->getStocks() as $stock) {
            $shipment = $context->storeToShipment[$stock->getStoreId()] ?? null;

            if ($shipment) {
                return $shipment;
            }
        }

        $delivery = $context->getNewDelivery();
        if (!$delivery) {
            throw new ValidateException("Для товара {$itemData->offerId} не найдена доступная доставка");
        }

        $stocks = $productInfo->offer->getStocks();
        /** @var Stock $firstStock */
        $firstStock = reset($stocks);
        $storeId = $firstStock->getStoreId();
        if (!$storeId) {
            throw new ValidateException("Для товара {$itemData->offerId} не найден склад для создания новой отгрузки");
        }

        $shipment = $this->makeShipmentAction->execute($productInfo->offer->getSellerId(), $storeId, $delivery);
        $context->storeToShipment[$shipment->store_id] = $shipment;

        return $shipment;
    }
}
