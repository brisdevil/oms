<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data;

use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Support\CalcPrice;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Illuminate\Support\Collection;

class CommitData
{
    public int $customerId;
    public string $customerEmail;
    public ?string $clientComment;

    public OrderSourceEnum $source;
    public PaymentSystemEnum $paymentSystemId;
    public ?int $spentBonus;
    public ?int $addedBonus;
    public ?string $promoCode;

    public int $deliveryService;
    public int $deliveryMethod;
    public ?int $deliveryPointId;
    public ?DeliveryAddress $deliveryAddress = null;
    public ?string $deliveryComment;
    public int $deliveryCost;
    public int $deliveryPrice;

    public string $receiverName;
    public string $receiverPhone;
    public string $receiverEmail;

    /** @var Collection<OrderItemData> */
    public Collection $items;

    /** @var Collection<DeliveryData> */
    public Collection $deliveries;

    public function price(): int
    {
        $itemsPrice = $this->items()->sum(fn (OrderItemData $item) => CalcPrice::itemPrice($item->qty, $item->pricePerOne));

        // TODO:deliveryPrice null
        return (int)round($itemsPrice) + ($this->deliveryPrice ?? 0);
    }

    public function cost(): int
    {
        $itemsCost = $this->items()->sum(fn (OrderItemData $item) => CalcPrice::itemPrice($item->qty, $item->costPerOne));

        // TODO:deliveryCost null
        return (int)round($itemsCost) + ($this->deliveryCost ?? 0);
    }

    /**
     * @return Collection<OrderItemData>
     */
    public function items(): Collection
    {
        if (!empty($this->items)) {
            return $this->items;
        }

        return $this->deliveries
            ->pluck('shipments')
            ->collapse()
            ->pluck('items')
            ->collapse();
    }
}
