<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data;

use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;

class ShipmentData
{
    public int $sellerId;
    public int $storeId;
    /** @var OrderItemData[] */
    public array $items;
}
