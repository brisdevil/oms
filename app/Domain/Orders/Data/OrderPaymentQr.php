<?php

namespace App\Domain\Orders\Data;

class OrderPaymentQr
{
    public const DEFAULT_PATH = '/tmp/';
    public const BASE_FILE_NAME = 'order_payment_qr_code';
    public const DIR_NAME = 'payment/qr_codes';
}
