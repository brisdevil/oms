<?php

namespace App\Domain\Orders\Models\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersResponse;

class OfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'product_id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'external_id' => $this->faker->uuid(),
            'storage_address' => $this->faker->nullable()->address,
            'sale_status' => $this->faker->randomElement(OfferSaleStatusEnum::getAllowableEnumValues()),
            'base_price' => $this->faker->optional()->numberBetween(1, 100_000) + 1,

            'stocks' => $this->faker->randomList(fn () => StockFactory::new()->make(), 1),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponseSearch(
        array $extras = [],
        int $count = 1,
        mixed $pagination = null,
        ?callable $beforeCallback = null
    ): SearchOffersResponse {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination, $beforeCallback);
    }

    public function makeResponseForIds(array $ids, array $extra = [], int $count = 1): SearchOffersResponse
    {
        $response = $this->makeResponseSearch($extra, $count);
        $data = $response->getData();
        foreach ($data as $key => $item) {
            $item->setId($ids[$key]);
        }

        $response->setData($data);

        return $response;
    }
}
