<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Models\Tests\Factories\OrderItemFactory;
use App\Domain\Refunds\Models\RefundOrderItem;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrderItem
 * @package App\Models
 *
 * @property int $id - id элемента корзины
 * @property int $order_id - id заказа
 * @property int $shipment_id - id отгрузки
 * @property int $offer_id - id предложения продавца
 * @property string $name - название товара
 * @property float $product_weight - вес нетто ед. товара (кг)
 * @property float $product_weight_gross - вес брутто ед. товара (кг)
 * @property float $product_width - ширина ед. товара (мм)
 * @property float $product_height - высота ед. товара (мм)
 * @property float $product_length - длина ед. товара (мм)
 * @property string|null $product_barcode - артикул (EAN) товара
 * @property int $offer_store_id - склад хранения оффера
 * @property string|null $offer_storage_address - адрес хранения товара в магазине
 * @property float $qty - кол-во товара
 * @property float|null $old_qty - кол-во товара(на момент оформления заказа)
 * @property int $price - цена элемента корзины со скидкой (price_per_one * qty)
 * @property int $price_per_one - цена единичного элемента корзины со скидкой
 * @property int $cost - стоимость элемента корзины без скидок (cost_per_one * qty)
 * @property int $cost_per_one - стоимость единичного элемента корзины без скидок
 * @property CarbonInterface|null $created_at - дата добавления
 * @property CarbonInterface|null $updated_at - дата обновления
 * @property bool $is_added - флаг, что товар был добавлен
 * @property bool $is_deleted - флаг, что товар был удалён
 *
 * @property-read RefundOrderItem|null $refundItem - информация о возврате элемента заказа
 * @property-read Order $order
 * @property-read Shipment $shipment
 */
class OrderItem extends Model implements Auditable
{
    use SupportsAudit;

    protected $casts = [
        'qty' => 'float',
        'old_qty' => 'float',
        'product_weight' => 'float',
        'product_weight_gross' => 'float',
        'product_width' => 'float',
        'product_height' => 'float',
        'product_length' => 'float',
    ];

    public static function factory(): OrderItemFactory
    {
        return OrderItemFactory::new();
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function shipment(): BelongsTo
    {
        return $this->belongsTo(Shipment::class);
    }

    public function scopeIsDelivered(Builder $query): Builder
    {
        return $query->whereHas('shipment', function (Builder $query) {
            return $query->whereHas('delivery', function (Builder $query) {
                $query->whereIn('status', DeliveryStatus::done());
            });
        });
    }

    public function getRealQty(): float
    {
        if ($this->is_deleted) {
            return 0;
        }

        return $this->qty;
    }
}
