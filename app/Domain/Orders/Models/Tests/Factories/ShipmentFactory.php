<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ShipmentFactory extends BaseModelFactory
{
    protected $model = Shipment::class;

    public function definition()
    {
        return [
            'delivery_id' => Delivery::factory(),
            'seller_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'status' => $this->faker->randomEnum(ShipmentStatusEnum::cases()),
            'number' => $this->faker->unique()->numerify('######-#-#'),
        ];
    }
}
