<?php

namespace App\Domain\Orders\Support;

class CalcPrice
{
    public static function itemPrice(float $qty, int $pricePerOne): int
    {
        return (int)round($qty * $pricePerOne);
    }
}
