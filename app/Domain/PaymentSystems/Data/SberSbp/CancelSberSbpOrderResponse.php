<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CancelSberSbpOrderResponse
{
    public string $operationDateTime;
    public string $operationType;
    public string $operationCurrency;
    public int $operationSum;
    public string $operationId;
    public string $orderId;

    public function __construct(array $response)
    {
        $this->operationDateTime = $response['operation_date_time'];
        $this->operationType = $response['operation_type'];
        $this->operationCurrency = $response['operation_currency'];
        $this->operationSum = $response['operation_sum'];
        $this->operationId = $response['operation_id'];
        $this->orderId = $response['order_id'];
    }
}
