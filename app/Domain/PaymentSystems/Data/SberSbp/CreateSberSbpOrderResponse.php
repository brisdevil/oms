<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CreateSberSbpOrderResponse
{
    public string $errorDescription;
    public string $orderNumber;
    public string $orderFormUrl;
    public string $errorCode;
    public string $orderId;
    public string $orderState;

    public function __construct(array $response)
    {
        $this->errorDescription = $response['error_description'];
        $this->orderNumber = $response['order_number'];
        $this->orderFormUrl = $response['order_form_url'];
        $this->errorCode = $response['error_code'];
        $this->orderId = $response['order_id'];
        $this->orderState = $response['order_state'];
    }
}
