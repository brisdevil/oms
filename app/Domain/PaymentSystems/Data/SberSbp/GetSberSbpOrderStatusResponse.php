<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

use Illuminate\Support\Collection;

class GetSberSbpOrderStatusResponse
{
    public string $idQr;
    public string $tid;
    public string $errorCode;
    public string $orderId;
    public string $orderState;
    /** @var Collection|OrderOperationParams[] */
    public Collection $orderOperationParams;

    public function __construct(array $response)
    {
        $this->idQr = $response['id_qr'];
        $this->tid = $response['tid'];
        $this->errorCode = $response['error_code'];
        $this->orderId = $response['order_id'];
        $this->orderState = $response['order_state'];

        $params = collect();

        foreach ($response['order_operation_params'] ?? [] as $operationParams) {
            $params->push(new OrderOperationParams($operationParams));
        }

        $this->orderOperationParams = $params;
    }
}
