<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class OrderOperationParams
{
    public string $operationId;
    public string $operationDateTime;
    public string $responseCode;
    public string $rrn;
    public string $authCode;
    public string $operationType;
    public string $operationCurrency;
    public int $operationSum;

    public function __construct(array $data = [])
    {
        $this->operationDateTime = $data['operation_date_time'];
        $this->responseCode = $data['response_code'];
        $this->operationSum = $data['operation_sum'];
        $this->operationType = $data['operation_type'];
        $this->operationId = $data['operation_id'];
        $this->operationCurrency = $data['operation_currency'];
        $this->rrn = $data['rrn'];
        $this->authCode = $data['auth_code'];
    }
}
