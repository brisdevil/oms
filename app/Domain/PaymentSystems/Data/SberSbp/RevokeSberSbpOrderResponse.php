<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class RevokeSberSbpOrderResponse
{
    public string $orderId;
    public string $orderState;

    public function __construct(array $response)
    {
        $this->orderId = $response['order_id'];
        $this->orderState = $response['order_state'];
    }
}
