<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

interface SberSbpRequest
{
    public function toArray(): array;
}
