<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class CancelTinkoffResponse extends AbstractTinkoffResponse
{
    public int $orderId;
    public string $status;
    public string $paymentId;
    public int $originalAmount;
    public int $newAmount;

    public function __construct(array $response)
    {
        parent::__construct($response);

        $this->orderId = $response['OrderId'];
        $this->status = $response['Status'];
        $this->paymentId = $response['PaymentId'];
        $this->originalAmount = $response['OriginalAmount'];
        $this->newAmount = $response['NewAmount'];
    }
}
