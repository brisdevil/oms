<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class ConfirmTinkoffResponse extends AbstractTinkoffResponse
{
    public int $orderId;
    public string $status;
    public string $paymentId;

    public function __construct(array $response)
    {
        parent::__construct($response);

        $this->orderId = $response['OrderId'];
        $this->status = $response['Status'];
        $this->paymentId = $response['PaymentId'];
    }
}
