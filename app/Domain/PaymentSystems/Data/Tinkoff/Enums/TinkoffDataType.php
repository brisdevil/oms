<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff\Enums;

enum TinkoffDataType: string
{
    case PAYLOAD = 'PAYLOAD';
    case IMAGE = 'IMAGE';
}
