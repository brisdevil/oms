<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class GetQrTinkoffRequest implements TinkoffRequest
{
    public string $paymentId;
    public string $dataType;

    public function toArray(): array
    {
        return [
            'PaymentId' => $this->paymentId,
            'DataType' => $this->dataType,
        ];
    }
}
