<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class InitTinkoffRequest implements TinkoffRequest
{
    public int $orderId;
    public int $amount;
    public ?string $description;
    public ?string $language;
    public ?string $successURL;
    public ?string $failURL;
    public ?string $payType;
    public ?array $receipt;

    public function toArray(): array
    {
        return [
            'OrderId' => $this->orderId,
            'Amount' => $this->amount,
            'Description' => $this->description,
            'Language' => $this->language,
            'SuccessURL' => $this->successURL,
            'FailURL' => $this->failURL,
            'PayType' => $this->payType,
            'Receipt' => $this->receipt,
        ];
    }
}
