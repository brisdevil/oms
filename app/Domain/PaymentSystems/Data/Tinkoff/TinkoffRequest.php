<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

interface TinkoffRequest
{
    public function toArray(): array;
}
