<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp;

use App\Domain\Orders\Actions\Payment\PaymentQr\GenerateQrCodeAction;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Data\SberSbp\CancelSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\CreateSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\GetSberSbpOrderStatusRequest;
use App\Domain\PaymentSystems\Data\SberSbp\RevokeSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOrderStatus;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;

class SberSbpPaymentSystem extends PaymentSystem
{
    protected LoggerInterface $logger;
    protected SberSbpClient $client;
    protected GenerateQrCodeAction $generateQrCodeAction;

    public function __construct()
    {
        $this->client = resolve(SberSbpClient::class);
        $this->generateQrCodeAction = resolve(GenerateQrCodeAction::class);
        $this->logger = logger()->channel('payments_sber_sbp');
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        if ($paymentData->qrCode) {
            $this->logger->warning('QR код для оплаты уже сгенерирован');

            return;
        }

        try {
            $paymentData->returnLink = $returnLink;

            $orderNumber = Order::makePaymentNumber($paymentData->orderId);

            $request = new CreateSberSbpOrderRequest();
            $request->orderNumber = $orderNumber;
            $request->orderSum = $paymentData->getFullPrice();
            $request->orderCreateDate = $paymentData->orderDate->format(SberSbpClient::DATE_TIME_FORMAT);
            $request->memberId = (string)$paymentData->customerId;

            $response = $this->client->create($request);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при регистрации заказа Сбер СБП', ['message' => $e->getMessage()]);

            throw $e;
        }

        $paymentData->externalId = $response->orderId;
        $paymentData->paymentLink = $response->orderFormUrl;
        $paymentData->qrCode = $this->generateQrCodeAction->execute($paymentData->orderId, $response->orderFormUrl);
    }

    public function commitHoldedPayment($paymentData)
    {
        throw new ValidateException("Нельзя произвести списание средств из HOLD в СБП. средства списываются при создании оплаты");
    }

    public function reinit($paymentData)
    {
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function revert($paymentData)
    {
        try {
            if (in_array($paymentData->status, PaymentStatus::cancelled())) {
                return;
            }

            if ($paymentData->status == PaymentStatusEnum::NOT_PAID) {
                $request = new RevokeSberSbpOrderRequest();
                $request->orderId = $paymentData->externalId;

                $this->client->revoke($request);

                $paymentData->setStatusCancelled();
            } elseif ($paymentData->status == PaymentStatusEnum::PAID) {
                $orderNumber = Order::makePaymentNumber($paymentData->orderId);

                $request = new CancelSberSbpOrderRequest();
                $request->orderId = $paymentData->externalId;
                $request->cancelOperationSum = $paymentData->getFullPrice();

                $this->client->cancel($request, $orderNumber);

                $paymentData->setStatusReturned();
            } else {
                $paymentData->setStatusCancelled();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function checkPayment($paymentData): void
    {
        if (!$paymentData->qrCode) {
            $msg = 'Не сгенерирован QR код';

            $this->logger->error($msg);

            throw new Exception($msg);
        }

        $orderNumber = Order::makePaymentNumber($paymentData->orderId);

        $request = new GetSberSbpOrderStatusRequest();
        $request->orderId = $paymentData->externalId;
        $request->partnerOrderNumber = $orderNumber;

        $response = $this->client->getStatus($request);

        $orderStatus = $response->orderState;

        if ($orderStatus == SberSbpOrderStatus::PAID->value) {
            $paymentData->setStatusPaid();
        }
    }
}
