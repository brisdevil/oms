<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff\Tests;

use App\Domain\PaymentSystems\Data\Tinkoff\CancelTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CancelTinkoffResponseFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'OrderId' => $this->faker->modelId(),
            'PaymentId' => $this->faker->uuid(),
            'Status' => $this->faker->randomEnum(TinkoffPaymentStatus::cases()),
            'Success' => $this->faker->boolean(),
            'OriginalAmount' => $this->faker->numberBetween(1),
            'NewAmount' => $this->faker->numberBetween(1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): CancelTinkoffResponse
    {
        return new CancelTinkoffResponse($this->makeArray($extra));
    }
}
