<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff\Tests;

use App\Domain\PaymentSystems\Data\Tinkoff\ConfirmTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ConfirmTinkoffResponseFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'OrderId' => $this->faker->modelId(),
            'PaymentId' => $this->faker->uuid(),
            'Status' => $this->faker->randomEnum(TinkoffPaymentStatus::cases()),
            'Success' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): ConfirmTinkoffResponse
    {
        return new ConfirmTinkoffResponse($this->makeArray($extra));
    }
}
