<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff;

use App\Domain\PaymentSystems\Data\Tinkoff\CancelTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\CancelTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\ConfirmTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\ConfirmTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrStateTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrStateTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\GetStateTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\GetStateTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffResponse;
use App\Domain\PaymentSystems\Data\Tinkoff\TinkoffRequest;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

class TinkoffClient
{
    private ClientInterface $client;
    private LoggerInterface $logger;

    private string $baseUrl;
    private string $terminalKey;
    private string $secret;

    public function __construct(ClientInterface $client = null)
    {
        $this->client = $client ?: new Client();
        $this->logger = logger()->channel('payments_tinkoff');

        $this->baseUrl = config('services.tinkoff.base_url');
        $this->terminalKey = config('services.tinkoff.terminal_key');
        $this->secret = config('services.tinkoff.secret_key');
    }

    public function init(InitTinkoffRequest $request): InitTinkoffResponse
    {
        return new InitTinkoffResponse($this->send('Init', $request));
    }

    public function getState(GetStateTinkoffRequest $request): GetStateTinkoffResponse
    {
        return new GetStateTinkoffResponse($this->send('GetState', $request));
    }

    public function confirm(ConfirmTinkoffRequest $request): ConfirmTinkoffResponse
    {
        return new ConfirmTinkoffResponse($this->send('Confirm', $request));
    }

    public function cancel(CancelTinkoffRequest $request): CancelTinkoffResponse
    {
        return new CancelTinkoffResponse($this->send('Cancel', $request));
    }

    public function getQr(GetQrTinkoffRequest $request): GetQrTinkoffResponse
    {
        return new GetQrTinkoffResponse($this->send('GetQr', $request));
    }

    public function getQrState(GetQrStateTinkoffRequest $request): GetQrStateTinkoffResponse
    {
        return new GetQrStateTinkoffResponse($this->send('GetQrState', $request));
    }

    private function generateToken(array $params): string
    {
        foreach (['Shops', 'Receipt', 'Data'] as $ignore_key) {
            if (isset($params[$ignore_key])) {
                unset($params[$ignore_key]);
            }
        }

        if (!empty($this->secret)) {
            $params['Password'] = $this->secret;
        }
        ksort($params);

        $token = '';
        foreach ($params as $param_value) {
            if (is_scalar($param_value)) {
                $token .= $param_value;
            }
        }

        return hash('sha256', $token);
    }

    private function send(string $method, TinkoffRequest $request): array
    {
        $data = $request->toArray();
        $data['TerminalKey'] = $this->terminalKey;
        $data['Token'] = $this->generateToken($data);

        $this->logger->info("Request start $method", $data);

        $result = $this->client->request('POST', $method, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'base_uri' => $this->baseUrl,
            'json' => $data,
        ]);

        $data = json_decode($result->getBody(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Невалидный ответ');
        }

        $this->logger->info("Request finish $method", $data);

        $success = isset($data['Success']) && $data['Success'];
        if (!$success) {
            $exceptionMessage = $data['Message'];
            if (isset($data['Details'])) {
                $exceptionMessage .= ': ' . $data['Details'];
            }
            if (isset($data['QrCancelCode'])) {
                $exceptionMessage .= ' ' . $data['QrCancelCode'];
            }
            if (isset($data['QrCancelMessage'])) {
                $exceptionMessage .= ': ' . $data['QrCancelMessage'];
            }

            throw new Exception($exceptionMessage, $data['ErrorCode']);
        }

        return $data;
    }
}
