<?php

namespace App\Domain\PaymentSystems\Systems\TinkoffSbp;

use App\Domain\PaymentSystems\Systems\PaymentData;

class TinkoffSbpPaymentData extends PaymentData
{
    public ?string $returnLink;
    public ?string $qrCode;

    public function __construct(array $data, protected int $fullPrice, public int $deliveryPrice)
    {
        parent::__construct($data, $fullPrice, $deliveryPrice);
        $this->returnLink = $data['returnLink'] ?? null;
        $this->qrCode = $data['qrCode'] ?? null;
    }

    public function getData(): array
    {
        return [
            'returnLink' => $this->returnLink,
            'qrCode' => $this->qrCode,
            'amount' => $this->getFullPrice(),
        ];
    }
}
