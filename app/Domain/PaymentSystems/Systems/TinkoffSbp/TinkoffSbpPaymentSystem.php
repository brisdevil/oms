<?php

namespace App\Domain\PaymentSystems\Systems\TinkoffSbp;

use App\Domain\Orders\Actions\Payment\PaymentQr\GenerateQrCodeAction;
use App\Domain\PaymentSystems\Data\Tinkoff\CancelTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffDataType;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffLanguage;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentMethod;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentObject;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentType;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffTaxation;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffTaxationType;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrStateTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\GetQrTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffRequest;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Domain\PaymentSystems\Systems\ReceiptItemData;
use App\Domain\PaymentSystems\Systems\Tinkoff\TinkoffClient;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;

class TinkoffSbpPaymentSystem extends PaymentSystem
{
    protected LoggerInterface $logger;

    private TinkoffClient $client;

    protected GenerateQrCodeAction $generateQrCodeAction;

    public function __construct()
    {
        $this->client = resolve(TinkoffClient::class);
        $this->logger = logger()->channel('payments_tinkoff_sbp');
        $this->generateQrCodeAction = resolve(GenerateQrCodeAction::class);
    }

    /**
     * @param TinkoffSbpPaymentData $paymentData
     * @throws Exception
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        $request = new InitTinkoffRequest();

        $request->amount = $paymentData->getFullPrice();
        $request->orderId = $paymentData->orderId;
        $request->language = TinkoffLanguage::LANGUAGE_RU->value;
        $request->payType = TinkoffPaymentType::PAY_TYPE_ONE_STAGE->value;
        $request->successURL = $returnLink;
        $request->failURL = $failLink;
        $request->description = "Заказ №{$paymentData->orderId}";
        $request->receipt = [
            'Email' => $paymentData->customerEmail ?? null,
            'Taxation' => TinkoffTaxationType::TAX_TYPE_USN_INCOME,
            'Items' => $this->generateItems($paymentData->receiptItems, $paymentData->deliveryPrice),
        ];

        try {
            $this->logger->info('Регистрация нового заказа в Тинькофф СБП');

            $response = $this->client->init($request);
            $this->logger->info('Ответ от Init', ['id' => $response->paymentId, 'status' => $response->status]);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе Init', ['message' => $e->getMessage()]);

            throw $e;
        }

        $getQrRequest = new GetQrTinkoffRequest();
        $getQrRequest->paymentId = $response->paymentId;
        $getQrRequest->dataType = TinkoffDataType::PAYLOAD->value;

        try {
            $this->logger->info('Регистрация QR в Тинькофф СБП');

            $qrResponse = $this->client->getQr($getQrRequest);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе GetQr', ['message' => $e->getMessage()]);

            throw $e;
        }

        $paymentData->externalId = $response->paymentId;
        $paymentData->paymentLink = $response->paymentURL;
        $paymentData->qrCode = $this->generateQrCodeAction->execute($paymentData->orderId, $qrResponse->data);
    }

    /**
     * @param TinkoffSbpPaymentData $paymentData
     * @throws Exception
     */
    public function commitHoldedPayment($paymentData): void
    {
        throw new ValidateException("Нельзя произвести списание средств из HOLD в СБП. средства списываются при создании оплаты");
    }

    public function reinit($paymentData)
    {
    }

    /**
     * @param TinkoffSbpPaymentData $paymentData
     * @throws Exception
     */
    public function revert($paymentData): void
    {
        $this->logger->info('Попытка отмены платежа', [
            'payment_external_id' => $paymentData->externalId,
        ]);

        $request = new CancelTinkoffRequest();
        $request->paymentId = $paymentData->externalId;

        try {
            $this->client->cancel($request);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе Cancel', ['message' => $e->getMessage()]);

            throw $e;
        }

        if ($paymentData->status == PaymentStatusEnum::HOLD) {
            $paymentData->setStatusCancelled();

            return;
        }

        if ($paymentData->status == PaymentStatusEnum::PAID) {
            $paymentData->setStatusReturned();

            return;
        }

        $paymentData->setStatusCancelled();
    }

    /**
     * @param TinkoffSbpPaymentData $paymentData
     * @throws Exception
     */
    public function checkPayment($paymentData): void
    {
        if (!$paymentData->externalId) {
            $msg = 'Не указан payment_external_id';

            $this->logger->error($msg);

            throw new Exception($msg);
        }

        $request = new GetQrStateTinkoffRequest();
        $request->paymentId = $paymentData->externalId;

        try {
            $response = $this->client->getQrState($request);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе GetQrState', ['message' => $e->getMessage()]);

            throw $e;
        }

        $this->logger->info('Ответ GetQrState', ['id' => $paymentData->externalId, 'status' => $response->status]);

        if ($response->status == TinkoffPaymentStatus::STATUS_AUTHORIZED->value) {
            $paymentData->setStatusHold();
        }

        if ($response->status == TinkoffPaymentStatus::STATUS_CONFIRMED->value) {
            $paymentData->setStatusPaid();
        }

        if (in_array($response->status, [TinkoffPaymentStatus::STATUS_PARTIAL_REVERSED->value, TinkoffPaymentStatus::STATUS_REVERSED->value])) {
            $paymentData->setStatusCancelled();
        }

        if (in_array($response->status, [TinkoffPaymentStatus::STATUS_PARTIAL_REFUNDED->value, TinkoffPaymentStatus::STATUS_REFUNDED->value])) {
            $paymentData->setStatusReturned();
        }
    }

    /**
     * @param ReceiptItemData[] $receiptItems
     * @return array
     */
    protected function generateItems(array $receiptItems, int $deliveryPrice): array
    {
        $items = [];
        foreach ($receiptItems as $receiptItem) {
            $items[] = [
                'Name' => $receiptItem->name,
                'Quantity' => $receiptItem->qty,
                'Amount' => $receiptItem->price,
                'Price' => $receiptItem->pricePerOne,
                'PaymentMethod' => TinkoffPaymentMethod::FULL_PREPAYMENT,
                'PaymentObject' => TinkoffPaymentObject::COMMODITY,
                'Tax' => TinkoffTaxation::NONE,
            ];
        }

        if ($deliveryPrice > 0) {
            $items[] = [
                'Name' => 'Доставка',
                'Quantity' => 1,
                'Amount' => $deliveryPrice,
                'Price' => $deliveryPrice,
                'PaymentMethod' => TinkoffPaymentMethod::FULL_PREPAYMENT,
                'PaymentObject' => TinkoffPaymentObject::SERVICE,
                'Tax' => TinkoffTaxation::VAT20,
            ];
        }

        return $items;
    }
}
