<?php

namespace App\Domain\Refunds\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $refund_id Идентификатор заявки на возврат
 * @property int $order_item_id Идентификатор элемента заказа
 * @property float $qty Количество возвращаемых элементов заказа
 */
class RefundOrderItem extends Pivot
{
    protected $casts = [
        'qty' => 'float',
    ];
}
