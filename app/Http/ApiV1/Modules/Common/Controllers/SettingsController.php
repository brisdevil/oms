<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Actions\PatchSeveralSettingsAction;
use App\Http\ApiV1\Modules\Common\Queries\SettingsQuery;
use App\Http\ApiV1\Modules\Common\Requests\PatchSettingsRequest;
use App\Http\ApiV1\Modules\Common\Resources\SettingsResource;
use Illuminate\Contracts\Support\Responsable;

class SettingsController
{
    public function search(SettingsQuery $query): Responsable
    {
        return SettingsResource::collection($query->get());
    }

    public function patchSeveral(PatchSettingsRequest $request, PatchSeveralSettingsAction $action): Responsable
    {
        return SettingsResource::collection($action->execute($request->validated()));
    }
}
