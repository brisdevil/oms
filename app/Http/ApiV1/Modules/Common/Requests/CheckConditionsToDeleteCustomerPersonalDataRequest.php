<?php

namespace App\Http\ApiV1\Modules\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckConditionsToDeleteCustomerPersonalDataRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->input('customer_id');
    }
}
