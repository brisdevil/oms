<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeOrderItemQtyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'order_items' => ['required', 'array'],
            'order_items.*.item_id' => ['required', 'integer'],
            'order_items.*.qty' => ['required', 'numeric', 'gt:0'],
        ];
    }
}
