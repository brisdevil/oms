<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\CommitData;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\DeliveryData;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\ShipmentData;
use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\Timeslot;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CommitOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge([
            'customer_id' => ['required', 'integer'],
            'client_comment' => ['nullable', 'string'],
//            TODO: скрыто в рамках задачи #90290
            'customer_email' => ['required', 'string'],
            'payment_system_id' => ['required', new Enum(PaymentSystemEnum::class)],
            'source' => ['required', new Enum(OrderSourceEnum::class)],
//            'spent_bonus' => ['nullable', 'integer'],
//            'added_bonus' => ['nullable', 'integer'],
            'promo_code' => ['nullable', 'string'],

            'delivery_method' => ['required', 'integer', Rule::in(DeliveryMethodEnum::getAllowableEnumValues())],
            'delivery_comment' => ['nullable', 'string'],
            'delivery_address' => [
                'nullable',
                'array',
                'required_if:delivery_method,' . DeliveryMethodEnum::DELIVERY,
                'prohibited_if:delivery_method,' . DeliveryMethodEnum::PICKUP,
            ],

//            'delivery_service' => ['required', 'integer'],
//            'delivery_point_id' => ['nullable', 'integer'],
            'delivery_price' => ['required', 'integer', 'min:0'],
//            'delivery_cost' => ['required', 'integer', 'min:0'],
            'receiver_name' => ['required', 'string'],
            'receiver_phone' => ['required', new PhoneRule()],
            'receiver_email' => ['required', 'string'],

            'deliveries' => ['required', 'array'],
//            'deliveries.*.date' => ['required', 'date_format:Y-m-d'],
//            'deliveries.*.timeslot' => ['nullable', 'array'],
//            'deliveries.*.cost' => ['required', 'integer', 'min:0'],
            'deliveries.*.shipments' => ['required', 'array'],
            'deliveries.*.shipments.*.seller_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.store_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.items' => ['required', 'array'],
            'deliveries.*.shipments.*.items.*.offer_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.items.*.cost_per_one' => ['required', 'numeric', 'min:0'],
            'deliveries.*.shipments.*.items.*.price_per_one' => ['required', 'numeric', 'min:0'],
            'deliveries.*.shipments.*.items.*.qty' => ['required', 'numeric', 'min:0'],
        ]);
    }

    public function convertToObject(): CommitData
    {
        $order = new CommitData();
        $order->customerId = $this->get('customer_id');
        $order->clientComment = $this->get('client_comment');
        $order->customerEmail = $this->get('customer_email');
        $order->paymentSystemId = PaymentSystemEnum::from($this->get('payment_system_id'));
        $order->source = OrderSourceEnum::from($this->get('source'));
        $order->spentBonus = $this->get('spent_bonus');
        $order->addedBonus = $this->get('added_bonus');
        $order->promoCode = $this->get('promo_code');

        $order->deliveryMethod = $this->get('delivery_method');
        $order->deliveryComment = $this->get('delivery_comment');

        $deliveryAddress = $this->get('delivery_address');
        if ($deliveryAddress) {
            // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
            $order->deliveryAddress = DeliveryAddress::validateOrFail($deliveryAddress);
        }

        $order->deliveryService = $this->get('delivery_service', 1); //todo
        $order->deliveryPointId = $this->get('delivery_point_id');

        $order->deliveryPrice = $this->get('delivery_price', 0);
        $order->deliveryCost = $this->get('delivery_cost', 0); //todo

        $order->receiverName = $this->get('receiver_name');
        $order->receiverPhone = $this->get('receiver_phone');
        $order->receiverEmail = $this->get('receiver_email');

        $order->deliveries = $this->getDeliveries();

        return $order;
    }

    protected function getDeliveries(): Collection
    {
        $deliveries = collect();
        foreach ($this->get('deliveries') as $delivery) {
            $deliveryData = new DeliveryData();
            $deliveryData->date = Date::createFromFormat('Y-m-d', $delivery['date'] ?? now()->addDay()->format('Y-m-d'))->startOfDay();   //todo
            $timeslot = $delivery['timeslot'] ?? ["id" => 'id', "from" => '10:00', "to" => '12:00',]; //todo
            if ($timeslot) {
                // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
                $deliveryData->timeslot = Timeslot::validateOrFail($timeslot);
            }
            $deliveryData->cost = $delivery['cost'] ?? 0;  //todo

            foreach ($delivery['shipments'] as $shipment) {
                $shipmentData = new ShipmentData();
                $shipmentData->sellerId = $shipment['seller_id'];
                $shipmentData->storeId = $shipment['store_id'];

                foreach ($shipment['items'] as $items) {
                    $itemData = new OrderItemData();
                    $itemData->offerId = $items['offer_id'];
                    $itemData->costPerOne = $items['cost_per_one'];
                    $itemData->pricePerOne = $items['price_per_one'];
                    $itemData->qty = $items['qty'];

                    $shipmentData->items[] = $itemData;
                }

                $deliveryData->shipments[] = $shipmentData;
            }

            $deliveries->push($deliveryData);
        }

        return $deliveries;
    }
}
