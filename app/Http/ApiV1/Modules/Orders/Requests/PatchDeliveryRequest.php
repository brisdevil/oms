<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchDeliveryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'date' => ['sometimes', 'date'],
            'timeslot' => ['nullable', 'array'],
            'status' => ['nullable', Rule::enum(DeliveryStatusEnum::class)],
        ];
    }
}
