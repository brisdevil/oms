<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Order
 */
class CommitOrderResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'order_id' => $this->id,
            'payment_link' => $this->whenNotNull($this->payment_link),
        ];
    }
}
