<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\OrderPaymentData;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Order
 */
class OrdersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'number' => $this->number,

            'responsible_id' => $this->responsible_id,

            'customer_id' => $this->customer_id,
            'customer_email' => $this->customer_email,

            'status' => $this->status,
            'status_at' => $this->status_at,

            'cost' => $this->cost,
            'price' => $this->price,

            'delivery_service' => $this->delivery_service,
            'delivery_method' => $this->delivery_method,
            'delivery_cost' => $this->delivery_cost,
            'delivery_price' => $this->delivery_price,
            'delivery_point_id' => $this->delivery_point_id,
            'delivery_address' => $this->delivery_address,
            'delivery_comment' => $this->delivery_comment,

            'receiver_name' => $this->receiver_name,
            'receiver_phone' => $this->receiver_phone,
            'receiver_email' => $this->receiver_email,

            'spent_bonus' => $this->spent_bonus,
            'added_bonus' => $this->added_bonus,
            'promo_code' => $this->promo_code,

            'source' => $this->source,
            'payment_status' => $this->payment_status,
            'payment_status_at' => $this->payment_status_at,
            'payed_at' => $this->payed_at,
            'payed_price' => $this->payed_price,
            'payment_system' => $this->payment_system,
            'payment_method' => $this->payment_method,
            'payment_expires_at' => $this->payment_expires_at,
            'payment_link' => $this->payment_link,
            'payment_external_id' => $this->payment_external_id,
            'payment_data' => new OrderPaymentDataResource(new OrderPaymentData($this->payment_data)),

            'is_editable' => $this->isEditable(),
            'is_changed' => $this->is_changed,
            'is_expired' => $this->is_expired,
            'is_expired_at' => $this->is_expired_at,
            'is_return' => $this->is_return,
            'is_return_at' => $this->is_return_at,
            'is_partial_return' => $this->is_partial_return,
            'is_partial_return_at' => $this->is_partial_return_at,
            'is_problem' => $this->is_problem,
            'is_problem_at' => $this->is_problem_at,
            'problem_comment' => $this->problem_comment,

            'client_comment' => $this->client_comment,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'items' => OrderItemsResource::collection($this->whenLoaded('items')),
            'deliveries' => DeliveriesResource::collection($this->whenLoaded('deliveries')),
            'files' => OrderFilesResource::collection($this->whenLoaded('files')),
        ];
    }
}
