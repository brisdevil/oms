<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;

class OrderCommitRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $deliveryPrice = $this->faker->nullable(default: 0)->numberBetween(1, 200);

        //TODO: скрыты поля
        return [
            'source' => $this->faker->randomEnum(OrderSourceEnum::cases()),

            'customer_id' => $this->faker->modelId(),
            'customer_email' => $this->faker->email(),
            'client_comment' => $this->faker->nullable()->text(50),
            'payment_system_id' => $this->faker->randomEnum(PaymentSystemEnum::cases()),
//            'spent_bonus' => $this->faker->nullable()->numberBetween(0, 10),
//            'added_bonus' => $this->faker->nullable()->numberBetween(0, 10),
            'promo_code' => $this->faker->nullable()->word(),
//
//            "delivery_service" => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_method' => $deliveryMethod,
//            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_address' => $isDelivery ? DeliveryAddress::factory()->make()->toArray() : null,
            'delivery_comment' => $isDelivery ? $this->faker->nullable()->text(50) : null,
            "delivery_price" => $deliveryPrice,
//            "delivery_cost" => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
//
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),

            'deliveries' => $this->definitionDeliveries(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    protected function definitionItems(): array
    {
        $items = [];
        for ($k = 0; $k < $this->faker->numberBetween(1, 3); $k++) {
            $pricePerOne = $this->faker->numberBetween(1, 1000);
            $items[] = [
                'offer_id' => $this->faker->unique()->modelId(),
                'price_per_one' => $pricePerOne,
                'cost_per_one' => $this->faker->numberBetween($pricePerOne, $pricePerOne + 500),
                'qty' => $this->faker->randomFloat(2, 1, 1000),
            ];
        }

        return $items;
    }

    protected function definitionDeliveries(): array
    {
        $deliveries = [];
        for ($i = 0; $i < $this->faker->numberBetween(1, 3); $i++) {
            $shipments = [];
            for ($j = 0; $j < $this->faker->numberBetween(1, 3); $j++) {
                $items = $this->definitionItems();

                $shipments[] = [
                    'seller_id' => $this->faker->modelId(),
                    'store_id' => $this->faker->modelId(),
                    'items' => $items,
                ];
            }
            $deliveries[] = [
//                'date' => $this->faker->date(),
//                'timeslot' => $this->faker->boolean() ? Timeslot::factory()->make()->toArray() : null,
//                'cost' => $this->faker->numberBetween(1, 1000),
                'shipments' => $shipments,
            ];
        }

        return $deliveries;
    }
}
