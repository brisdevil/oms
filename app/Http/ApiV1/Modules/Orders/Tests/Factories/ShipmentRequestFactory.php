<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ShipmentRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'number' => $this->faker->numerify('######-#-#'),
            'status' => $this->faker->randomEnum(ShipmentStatusEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
