<?php

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderFile;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\TestFactories\FakerProvider;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\post;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/orders/orders/{id}:attach-file 201', function (string $ext, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);

    /** @var Order $order */
    $order = Order::factory()->create();

    $requestBody = ['file' => UploadedFile::fake()->create("file.{$ext}", 100)];

    $response = post("/api/v1/orders/orders/{$order->id}:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(201);

    $responseFile = $response->decodeResponseJson()['data']['file'];

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertExists($responseFile['path']);
    assertDatabaseHas((new OrderFile())->getTable(), [
        'order_id' => $order->id,
        'path' => $responseFile['path'],
    ]);
})->with(
    ['jpg', 'pdf'],
    FakerProvider::$optionalDataset
);

test('POST /api/v1/orders/orders/{id}:attach-file 404', function () {
    $requestBody = ['file' => UploadedFile::fake()->create("file.jpg", 100)];
    post("/api/v1/orders/orders/100:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])->assertStatus(404);
});

test('DELETE /api/v1/orders/orders/{id}:delete-files success', function (bool $fileExists, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $filePath = "order_files/file.jpg";
    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);
    if ($fileExists) {
        Storage::disk($diskName)->put($filePath, 'some content');
    }
    /** @var Order $order */
    $order = Order::factory()->create();
    /** @var OrderFile $orderFile */
    $orderFile = OrderFile::factory()->for($order)->withPath($filePath)->create();

    deleteJson("/api/v1/orders/orders/{$order->id}:delete-files", ['file_ids' => [$orderFile->id]])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertMissing($filePath);
    assertDatabaseMissing((new OrderFile())->getTable(), [
        'order_id' => $order->id,
    ]);
})->with(
    [true, false],
    FakerProvider::$optionalDataset
);

test('DELETE /api/v1/orders/orders/{id}:delete-files bad', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order1 */
    $order1 = Order::factory()->create();
    /** @var Order $order2 */
    $order2 = Order::factory()->create();
    /** @var OrderFile $orderFile */
    $orderFile = OrderFile::factory()->for($order1)->create();

    deleteJson("/api/v1/orders/orders/{$order2->id}:delete-files", ['file_ids' => [$orderFile->id]])->assertStatus(400);
})->with(FakerProvider::$optionalDataset);
