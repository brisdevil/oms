<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderComment;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Support\CalcPrice;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffRequest;
use App\Domain\PaymentSystems\Systems\Sber\Tests\RegisterOrderPreAuthFactory;
use App\Domain\PaymentSystems\Systems\SberSbp\Tests\CreateSberSbpOrderFactory;
use App\Domain\PaymentSystems\Systems\Tinkoff\Tests\InitTinkoffResponseFactory;
use App\Domain\PaymentSystems\Systems\TinkoffSbp\Tests\GetQrTinkoffFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\OrderCommitRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\OrderRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Ensi\TestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region orders/orders:search
test("POST /api/v1/orders/orders:search success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $orders = Order::factory()
        ->count(10)
        ->sequence(
            ['status' => OrderStatusEnum::DONE],
            ['status' => OrderStatusEnum::NEW],
        )
        ->create();

    postJson("/api/v1/orders/orders:search", [
        "filter" => ["status" => OrderStatusEnum::NEW],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $orders->last()->id)
        ->assertJsonPath('data.0.status', OrderStatusEnum::NEW->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/orders/orders:search filter manager_comment_like success", function () {
    $comment = 'manager_comment_like';
    /** @var Order $order */
    $order = Order::factory()->create();
    OrderComment::factory()->for($order)->create(['text' => $comment]);
    Order::factory()->create();

    postJson("/api/v1/orders/orders:search", ["filter" => ["manager_comment_like" => $comment]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $order->id);
});

test("POST /api/v1/orders/orders:search filter is_editable success", function (
    OrderStatusEnum $status,
    PaymentMethodEnum $paymentMethod,
    PaymentSystemEnum $paymentSystem,
    bool $isEditable,
    int $count,
) {
    Order::factory()->create([
        'status' => $status,
        'payment_system' => $paymentSystem,
        'payment_method' => $paymentMethod,
    ]);

    postJson("/api/v1/orders/orders:search", ["filter" => ["is_editable" => $isEditable]])
        ->assertStatus(200)
        ->assertJsonCount($count, 'data');
})->with([
    [OrderStatusEnum::NEW, PaymentMethodEnum::OFFLINE, PaymentSystemEnum::CASH, true, 1],
    [OrderStatusEnum::APPROVED, PaymentMethodEnum::OFFLINE, PaymentSystemEnum::CASH, true, 0],
    [OrderStatusEnum::APPROVED, PaymentMethodEnum::OFFLINE, PaymentSystemEnum::CASH, false, 1],
    [OrderStatusEnum::NEW, PaymentMethodEnum::ONLINE, PaymentSystemEnum::SBER, true, 1],
    [OrderStatusEnum::NEW, PaymentMethodEnum::ONLINE, PaymentSystemEnum::TINKOFF, true, 1],
    [OrderStatusEnum::NEW, PaymentMethodEnum::ONLINE, PaymentSystemEnum::SBER_SBP, true, 0],
    [OrderStatusEnum::NEW, PaymentMethodEnum::ONLINE, PaymentSystemEnum::SBER_SBP, false, 1],
]);

test("POST /api/v1/orders/orders:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null,
    ?bool $always = null
) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/orders/orders:search", [
        "filter" => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $order->{$fieldKey}),
        ],
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $order->id);
})->with([
    ['number', '100', 'number_like', '1'],
    ['source', 1, 'source', [1, 2]],
    ['customer_id', 1, 'customer_id', [1, 2]],
    ['responsible_id', 1, 'responsible_id', [1, 2]],
    ['customer_email', 'test@tmp.ru', 'customer_email_like', 'tmp'],
    ['cost', 5, 'cost_gte', 4],
    ['cost', 5, 'cost_lte', 6],
    ['price', 5, 'price_gte', 4],
    ['price', 5, 'price_lte', 6],
    ['spent_bonus', 5, 'spent_bonus_gte', 4],
    ['spent_bonus', 5, 'spent_bonus_lte', 6],
    ['added_bonus', 5, 'added_bonus_gte', 4],
    ['added_bonus', 5, 'added_bonus_lte', 6],
    ['promo_code', 'newyear2021', 'promo_code_like', 'ewy'],
    ['delivery_method', 1, 'delivery_method', [1, 2]],
    ['delivery_service', 1, 'delivery_service', [1, 2]],
    ['delivery_point_id', 1, 'delivery_point_id', [1, 2]],
    ['delivery_price', 5, 'delivery_price_gte', 4],
    ['delivery_price', 5, 'delivery_price_lte', 6],
    ['delivery_cost', 5, 'delivery_cost_gte', 4],
    ['delivery_cost', 5, 'delivery_cost_lte', 6],
    ['receiver_name', 'Имя Фамилия Отчество', 'receiver_name_like', 'отче'],
    ['receiver_phone', '+799988867', 'receiver_phone_like', '867'],
    ['receiver_email', 'test@tmp.ru', 'receiver_email_like', 'tmp'],
    ['status', 1, 'status', [1, 2]],
    ['status_at', now()->subDay(), 'status_at_gte', now()->subDays(2)],
    ['status_at', now()->subDay(), 'status_at_lte', now()->addDays(2)],
    ['payment_status', 1, 'payment_status', [1, 2]],
    ['payment_status_at', now()->subDay(), 'payment_status_at_gte', now()->subDays(2)],
    ['payment_status_at', now()->subDay(), 'payment_status_at_lte', now()->addDays(2)],
    ['payed_at', now()->subDay(), 'payed_at_gte', now()->subDays(2)],
    ['payed_at', now()->subDay(), 'payed_at_lte', now()->addDays(2)],
    ['payment_expires_at', now()->subDay(), 'payment_expires_at_gte', now()->subDays(2)],
    ['payment_expires_at', now()->subDay(), 'payment_expires_at_lte', now()->addDays(2)],
    ['payment_method', 1, 'payment_method', [1, 2]],
    ['payment_system', 3, 'payment_system', [3, 4]],
    ['payment_external_id', 'b0f-cs', 'payment_external_id_like', 'f-c'],
    ['is_expired'],
    ['is_expired_at', now()->subDay(), 'is_expired_at_gte', now()->subDays(2)],
    ['is_expired_at', now()->subDay(), 'is_expired_at_lte', now()->addDays(2)],
    ['is_return'],
    ['is_return_at', now()->subDay(), 'is_return_at_gte', now()->subDays(2)],
    ['is_return_at', now()->subDay(), 'is_return_at_lte', now()->addDays(2)],
    ['is_partial_return'],
    ['is_partial_return_at', now()->subDay(), 'is_partial_return_at_gte', now()->subDays(2)],
    ['is_partial_return_at', now()->subDay(), 'is_partial_return_at_lte', now()->addDays(2)],
    ['is_problem'],
    ['is_problem_at', now()->subDay(), 'is_problem_at_gte', now()->subDays(2)],
    ['is_problem_at', now()->subDay(), 'is_problem_at_lte', now()->addDays(2)],
    ['problem_comment', 'текст Проблемы', 'problem_comment_like', 'ст пр'],
    ['created_at', now()->subDay(), 'created_at_gte', now()->subDays(2)],
    ['created_at', now()->subDay(), 'created_at_lte', now()->addDays(2)],
    ['updated_at', now()->subDay(), 'updated_at_gte', now()->subDays(2)],
    ['updated_at', now()->subDay(), 'updated_at_lte', now()->addDays(2)],
    ['delivery_comment', 'текст Доставки', 'delivery_comment_like', 'дост'],
    ['client_comment', 'Клиент', 'client_comment_like', 'клие'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/orders/orders:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Order::factory()->create();
    postJson("/api/v1/orders/orders:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'number',
    'customer_email',
    'cost',
    'price',
    'spent_bonus',
    'added_bonus',
    'promo_code',
    'delivery_price',
    'delivery_cost',
    'receiver_name',
    'receiver_phone',
    'receiver_email',
    'status_at',
    'payment_status_at',
    'payed_at',
    'payment_expires_at',
    'is_expired',
    'is_expired_at',
    'is_return',
    'is_return_at',
    'is_partial_return',
    'is_partial_return_at',
    'is_problem',
    'is_problem_at',
    'created_at',
    'updated_at',
], FakerProvider::$optionalDataset);
// endregion

// region orders/orders/{id}
test("GET /api/v1/orders/orders/{id} success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create();
    $deliveries = Delivery::factory()->for($order)->count(3)->create();

    getJson("/api/v1/orders/orders/{$order->id}/?include=deliveries")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonCount($deliveries->count(), 'data.deliveries');
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/orders/orders/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $deliveryComment = "delivery comment";
    /** @var Order $order */
    $order = Order::factory()->create([
        'delivery_comment' => $deliveryComment,
        'client_comment' => "client comment",
        'status' => OrderStatusEnum::WAIT_PAY,
    ]);

    $orderData = OrderRequestFactory::new()->only(['client_comment'])->make(['client_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/orders/orders/{$order->id}", $orderData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonPath('data.client_comment', $orderData['client_comment'])
        ->assertJsonPath('data.delivery_comment', $deliveryComment);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'client_comment' => $orderData['client_comment'],
        'delivery_comment' => $deliveryComment,
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/orders/orders/{id} client comment bad', function () {

    /** @var ApiV1ComponentTestCase $this */
    $comment = 'init';
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::APPROVED, 'client_comment' => $comment]);
    $orderData = OrderRequestFactory::new()->only(['client_comment'])->make(['client_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/orders/orders/{$order->id}", $orderData)->assertStatus(400);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'client_comment' => $comment,
    ]);
});

test('PATCH /api/v1/orders/orders/{id} check payed_price', function (int $price, ?int $payedPrice, bool $saved) {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create([
        'status' => OrderStatusEnum::NEW,
        'price' => $price,
        'payed_price' => $payedPrice,
    ]);

    Event::fake();

    $newStatus = OrderStatusEnum::APPROVED;
    patchJson("/api/v1/orders/orders/{$order->id}", ['status' => $newStatus])
        ->assertStatus($saved ? 200 : 400);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => $saved ? $newStatus : $order->status,
    ]);
})->with([
    [100, null, true],
    [100, 100, true],
    [100, 101, true],
    [100, 99, false],
]);
// endregion

// region orders/orders/{id}:start-payment
test('POST /api/v1/orders/orders/{id}:start-payment success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:start-payment", ['return_url' => "https://my.site"])
        ->assertStatus(200);

    assertDatabaseMissing(Order::class, [
        'id' => $order->id,
        'payment_link' => null,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

// region orders/orders/{id}:change-delivery
test('POST /api/v1/orders/orders/{id}:change-delivery success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $deliveryPrice = 100000;
    $deliveryCost = 100000;

    /** @var Order $order */
    $order = Order::factory()->create([
        'delivery_price' => $deliveryPrice,
        'delivery_cost' => $deliveryCost,
        'payment_status' => PaymentStatusEnum::NOT_PAID,
        'status' => OrderStatusEnum::NEW,
    ]);

    $orderData = OrderRequestFactory::new()->only(['delivery_comment'])->make(['delivery_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonPath('data.delivery_comment', $orderData['delivery_comment'])
        ->assertJsonPath('data.delivery_price', $deliveryPrice)
        ->assertJsonPath('data.delivery_cost', $deliveryCost);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'delivery_comment' => $orderData['delivery_comment'],
        'delivery_price' => $deliveryPrice,
        'delivery_cost' => $deliveryCost,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/orders/{id}:change-delivery bad payment status', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create([
        'payment_status' => PaymentStatusEnum::PAID,
    ]);

    $orderData = OrderRequestFactory::new()->makeChangeDelivery();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/orders/{id}:change-delivery bad order status', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create([
        'status' => OrderStatusEnum::APPROVED,
    ]);

    $orderData = OrderRequestFactory::new()->makeChangeDelivery();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);
// endregion

// region orders/orders/{id}:change-payment-method
test('POST /api/v1/orders/orders/{id}:change-payment-method success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-payment-method", ['payment_system' => PaymentSystemEnum::CASH->value])
        ->assertStatus(200);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'payment_system' => PaymentSystemEnum::CASH->value,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/orders/{id}:change-payment-method bad', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->withPayed()->withCash()->create();

    postJson("/api/v1/orders/orders/{$order->id}:change-payment-method", ['payment_system' => PaymentSystemEnum::TEST->value])
        ->assertStatus(400);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'payment_system' => PaymentSystemEnum::CASH->value,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

// region orders/orders/{id}:comment
test('PUT /api/v1/orders/orders/{id}:comment success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create();
    $text = 'text';

    putJson("/api/v1/orders/orders/{$order->id}:comment", ['text' => $text])
        ->assertStatus(200);

    assertDatabaseHas(OrderComment::class, [
        'order_id' => $order->id,
        'text' => $text,
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion

// region orders/orders:commit
test('POST /api/v1/orders/orders:commit success', function (PaymentSystemEnum $paymentSystem, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    /** @var EnsiFilesystemManager $diskManager */
    $diskManager = resolve(EnsiFilesystemManager::class);
    Storage::fake($diskManager->protectedDiskName());

    $orderCommit = OrderCommitRequestFactory::new()->make(['payment_system_id' => $paymentSystem->value]);
    $items = collect($orderCommit['deliveries'])->pluck('shipments')->collapse()->pluck('items')->collapse();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([
            'data' => $items->map(function (array $item, $key) {
                return new Offer([
                    'id' => $item['offer_id'],
                    'product_id' => $key + 1,
                    'external_id' => "{$item['offer_id']}_external",
                    'storage_address' => "storage_address_{$item['offer_id']}",
                    'stocks' => [
                        new Stock(['qty' => $item['qty'] + 1]),
                    ],
                ]);
            }),
        ]),
    ]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => new SearchProductsResponse([
            'data' => $items->map(function (array $item, $key) {
                $productId = $key + 1;

                return new Product([
                    'id' => $productId,
                    'name' => "product {$productId}",
                    'weight' => 100,
                    'width' => 150,
                    'weight_gross' => 150,
                    'barcode' => '15048765454',
                    'height' => 150,
                    'length' => 150,
                    'allow_publish' => true,
                    'order_minvol' => $item['qty'] - 1,
                ]);
            }),
        ]),
    ]);

    $this->mockOffersStocksApi()->shouldReceive('reserveStocks');

    if ($paymentSystem === PaymentSystemEnum::SBER) {
        $this->mockSberApi()->allows([
            'registerOrderPreAuth' => RegisterOrderPreAuthFactory::new()->make(),
        ]);
    }

    if ($paymentSystem === PaymentSystemEnum::SBER_SBP) {
        $this->mockSberSbpApi()->allows([
            'create' => CreateSberSbpOrderFactory::new()->makeResponse(),
        ]);
    }

    $tinkoffRequest = null;
    if (in_array($paymentSystem, [PaymentSystemEnum::TINKOFF, PaymentSystemEnum::TINKOFF_SBP])) {
        $mockTinkoffApi = $this->mockTinkoffApi();
        $mockTinkoffApi->shouldReceive('init')
            ->once()
            ->withArgs(function (InitTinkoffRequest $_request) use (&$tinkoffRequest) {
                $tinkoffRequest = $_request;

                return true;
            })
            ->andReturn(InitTinkoffResponseFactory::new()->makeResponse());

        if ($paymentSystem == PaymentSystemEnum::TINKOFF_SBP) {
            $mockTinkoffApi->shouldReceive('getQr')
                ->once()
                ->andReturn(GetQrTinkoffFactory::new()->makeResponse());
        }
    }

    postJson("/api/v1/orders/orders:commit", $orderCommit)
        ->assertStatus(201);

    $itemsPrice = $items->sum(fn ($item) => CalcPrice::itemPrice($item['qty'], $item['price_per_one']));
    $itemsCost = $items->sum(fn ($item) => CalcPrice::itemPrice($item['qty'], $item['cost_per_one']));

    // TODO: проверка без стоимости доставки
    assertDatabaseHas(Order::class, [
        'customer_id' => $orderCommit['customer_id'],
        'price' => $itemsPrice + $orderCommit['delivery_price'],
        'cost' => $itemsCost, // + $orderCommit['delivery_cost'],
    ]);

    if (!is_null($tinkoffRequest)) {
        assertEquals($itemsPrice + $orderCommit['delivery_price'], $tinkoffRequest->amount);
        assertCount(count($items) + ($orderCommit['delivery_price'] > 0 ? 1 : 0), $tinkoffRequest->receipt['Items']);
    }

    foreach ($items as $item) {
        assertDatabaseHas(OrderItem::class, [
            'offer_id' => $item['offer_id'],
            'qty' => $item['qty'],
            'old_qty' => null,
        ]);
    }
})->with(
    PaymentSystemEnum::cases(),
    FakerProvider::$optionalDataset,
);

test('POST /api/v1/orders/orders:commit inactive product', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderCommit = OrderCommitRequestFactory::new()->make();
    $items = collect($orderCommit['deliveries'])->pluck('shipments')->collapse()->pluck('items')->collapse();
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([
            'data' => $items->map(function (array $item, $key) {
                return new Offer([
                    'id' => $item['offer_id'],
                    'product_id' => $key + 1,
                    'external_id' => "{$item['offer_id']}_external",
                    'storage_address' => "storage_address_{$item['offer_id']}",
                    'stocks' => [
                        new Stock(['qty' => $item['qty'] + 1]),
                    ],
                ]);
            }),
        ]),
    ]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => new SearchProductsResponse([
            'data' => $items->map(function (array $item, $key) {
                $productId = $key + 1;

                return new Product([
                    'id' => $productId,
                    'name' => "product {$productId}",
                    'weight' => 100,
                    'width' => 150,
                    'weight_gross' => 150,
                    'barcode' => '15048765454',
                    'height' => 150,
                    'length' => 150,
                    'allow_publish' => false,
                    'order_minvol' => $item['qty'] - 1,
                ]);
            }),
        ]),
    ]);
    $this->mockOffersStocksApi()->shouldReceive('reserveStocks');

    postJson("/api/v1/orders/orders:commit", $orderCommit)
        ->assertStatus(400);

    assertDatabaseMissing(Order::class, [
        'customer_id' => $orderCommit['customer_id'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/orders/orders:commit less min qty', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderCommit = OrderCommitRequestFactory::new()->make();
    $items = collect($orderCommit['deliveries'])->pluck('shipments')->collapse()->pluck('items')->collapse();
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([
            'data' => $items->map(function (array $item, $key) {
                return new Offer([
                    'id' => $item['offer_id'],
                    'product_id' => $key + 1,
                    'external_id' => "{$item['offer_id']}_external",
                    'storage_address' => "storage_address_{$item['offer_id']}",
                    'stocks' => [
                        new Stock(['qty' => $item['qty'] + 1]),
                    ],
                ]);
            }),
        ]),
    ]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => new SearchProductsResponse([
            'data' => $items->map(function (array $item, $key) {
                $productId = $key + 1;

                return new Product([
                    'id' => $productId,
                    'name' => "product {$productId}",
                    'weight' => 100,
                    'width' => 150,
                    'weight_gross' => 150,
                    'barcode' => '15048765454',
                    'height' => 150,
                    'length' => 150,
                    'allow_publish' => true,
                    'type' => ProductTypeEnum::WEIGHT,
                    'order_minvol' => $item['qty'] + 1,
                ]);
            }),
        ]),
    ]);
    $this->mockOffersStocksApi()->shouldReceive('reserveStocks');

    postJson("/api/v1/orders/orders:commit", $orderCommit)
        ->assertStatus(400);

    assertDatabaseMissing(Order::class, [
        'customer_id' => $orderCommit['customer_id'],
    ]);
})->with(FakerProvider::$optionalDataset);
// endregion
