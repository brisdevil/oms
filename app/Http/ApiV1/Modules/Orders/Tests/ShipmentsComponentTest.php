<?php

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\ShipmentRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region orders/shipments:search
test("POST /api/v1/orders/shipments:search success", function () {
    $shipments = Shipment::factory()->count(3)->create();
    $filterId = $shipments->last()->id;

    postJson("/api/v1/orders/shipments:search", [
        "filter" => ["id" => $filterId],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $filterId);
});
// endregion
// region orders/shipments/{id}
test("GET /api/v1/orders/shipments/{id} success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create();
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->for($delivery)->create();
    $orderItems = OrderItem::factory()->for($shipment)->count(3)->create([
        'order_id' => $delivery->order_id,
        'is_deleted' => false,
    ]);

    getJson("/api/v1/orders/shipments/{$shipment->id}?include=delivery,order_items")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $shipment->id)
        ->assertJsonPath('data.delivery.id', $delivery->id)
        ->assertJsonCount($orderItems->count(), 'data.order_items');
})->with(FakerProvider::$optionalDataset);

test("PATCH /api/v1/orders/shipments/{id} success", function () {
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->create([
        'status' => ShipmentStatusEnum::NEW,
        'number' => '123',
    ]);
    $status = ShipmentStatusEnum::IN_WORK;

    $shipmentsData = ShipmentRequestFactory::new()->only(['status'])->make(['status' => $status]);

    patchJson("/api/v1/orders/shipments/{$shipment->id}", $shipmentsData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $shipment->id)
        ->assertJsonPath('data.status', $status->value)
        ->assertJsonPath('data.number', $shipment->number);

    assertDatabaseHas(Shipment::class, [
        'id' => $shipment->id,
        'status' => $status,
        'number' => $shipment->number,
    ]);
});
// endregion
