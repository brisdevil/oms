<?php

namespace App\Http\ApiV1\Modules\Refunds\Queries;

use App\Domain\Refunds\Models\Refund;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RefundsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Refund::query());

        $this->allowedIncludes(['order', 'items', 'reasons', 'files']);

        $this->allowedSorts(['id', 'order_id', 'source', 'status', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('order_id'),
            AllowedFilter::exact('manager_id'),
            AllowedFilter::exact('responsible_id'),
            AllowedFilter::exact('source'),
            AllowedFilter::exact('status'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
