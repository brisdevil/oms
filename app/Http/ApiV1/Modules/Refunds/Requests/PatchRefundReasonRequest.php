<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchRefundReasonRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'code' => ['string', 'max:20'],
            'name' => ['string', 'max:255'],
            'description' => ['string'],
        ];
    }
}
