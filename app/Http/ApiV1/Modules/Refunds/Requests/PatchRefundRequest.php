<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchRefundRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'responsible_id' => ['nullable', 'integer'],
            'status' => [new Enum(RefundStatusEnum::class)],
            'rejection_comment' => ['nullable', 'string'],
        ];
    }
}
