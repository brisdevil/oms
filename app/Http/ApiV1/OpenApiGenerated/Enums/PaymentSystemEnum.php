<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Система оплаты. Расшифровка значений:
 * * `3` - Тестовая система
 * * `4` - Наличные
 * * `5` - СБП
 * * `6` – Тинькофф онлайн
 * * `7` – Тинькофф СБП
 * * `8` - Сбер
 * * `9` - Сбер СБП
 */
enum PaymentSystemEnum: int
{
    /** Тестовая система */
    case TEST = 3;
    /** Наличные */
    case CASH = 4;
    /** СБП */
    case SBP = 5;
    /** Тинькофф онлайн */
    case TINKOFF = 6;
    /** Тинькофф СБП */
    case TINKOFF_SBP = 7;
    /** Сбер */
    case SBER = 8;
    /** Сбер СБП */
    case SBER_SBP = 9;
}
