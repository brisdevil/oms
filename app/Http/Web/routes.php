<?php

use App\Http\Web\Controllers\HealthCheck;
use App\Http\Web\Controllers\LocalPaymentController;
use App\Http\Web\Controllers\OasController;
use Illuminate\Support\Facades\Route;

Route::get('health', HealthCheck::class);

Route::get('/payment', [LocalPaymentController::class, 'index'])->name('paymentPage');
Route::get('/success-payed', [LocalPaymentController::class, 'successPayed'])->name('successPayedPage');
Route::get('/payment-failed', [LocalPaymentController::class, 'paymentFailed'])->name('paymentFailedPage');

Route::get('/', [OasController::class, 'list']);
