<?php

use Illuminate\Support\Str;

if (!function_exists('in_production')) {
    /**
     * Находится ли приложение в прод режиме
     */
    function in_production(): bool
    {
        return app()->environment('production');
    }
}

if (!function_exists('toPublicPath')) {
    /**
     * Изменить хост на публичный
     */
    function toPublicPath(string $url): string
    {
        if (!config('app.public_host')) {
            return $url;
        }

        return Str::replace(url()->to('/'), config('app.public_host'), $url);
    }
}
