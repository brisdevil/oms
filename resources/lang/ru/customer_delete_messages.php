<?php

return [
    'incomplete_orders' => 'Присутствуют не завершенные заказы.',
    'incomplete_refunds_in_order' => 'В заказе :orderId присутствуют не завершенные возвраты.',
    'incomplete_refund_time_in_order' => 'В заказе :orderId не прошло еще время осуществления возврата.',
];
